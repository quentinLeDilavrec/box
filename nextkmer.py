kmerfile=""
class Next:
    def __init__(self, kmerfile, k=31):
        self.kmerfile = kmerfile
        self.k = k

    def nextkmer(self, kmer):
        r = []
        tmp = kmer[1:self.k]
        with open(self.kmerfile) as f:
            for l in f:
                if tmp == l[:self.k-1]:
                    r.append(l)
        return r


    def getchain(self, kmer):
        tmp = kmer[:self.k]
        while 1:
            yield tmp
            lst=self.nextkmer(tmp)
            if len(lst)==0: return
            tmp=max(set(lst), key=lst.count)

    def getchains(self, kmer, i):
        tmp = kmer[-1]
        lst=self.nextkmer(tmp)
        if len(lst)==0 or i==0 : return kmer
        return [self.getchains(kmer+[e],i-1) for e in lst]
