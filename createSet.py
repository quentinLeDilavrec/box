from utils import lexico

def create_dBG_opti(In,k):
    """dont work"""
    raise Exception("Buged")
    dBG = set()
    with open(In, 'r', 6400) as f:
        tmp = ''
        while True:
            c = f.read(800)
            if c == '':
                break
            c = c.splitlines()
            if tmp!='' and c[0][0]!='>':
                dBG.add(lexico(curr))
                
            for e in c:
                if e[0]!='>':
                    dBG.add(lexico(curr))
                    curr = curr[1:]+c
                    c = f.read(1)
    return dBG


def create_dBG(In,k):
    '''
    return a set of kmer of size k from In.
    This version use by chuncks of letters.
    '''
    dBG = set()
    with open(In, 'r', 64000) as f:
        while True:
            c = f.read(1)
            if c == '':
                break
            if c == '>':
                while c!='\n':
                    c = f.read(1)
                    continue
            else:
                curr = c+f.read(k-1)
                while c!='\n':
                    dBG.add(lexico(curr))
                    curr = curr[1:]+c
                    c = f.read(1)
    return dBG


def create_dBG_chuncks(In,k):
    '''
    return a set of kmer of size k from In.
    This version read In line by line (can be slow on very long reads)
    '''
    dBG = set()
    with open(In) as f:
        for l in f:
            if l[0] == '>':
                continue
            for i in range(len(l)-k):
                curr = l[i:i+k]
                dBG.add(lexico(curr))
    return dBG
