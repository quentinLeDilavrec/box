import time
import sys

def reversecomp(s):
    '''
    return the reverse complement (A<->C and T<->G the reverse) of the sequence s
    '''
    return ''.join(reversed(['A' if e=='C' else 'C' if e=='A' else 'T' if e=='G' else 'G' for e in s]))

def lexico(s):
    '''
    return the sequence of ACTG in the lexicographic order
    '''
    tmp = reversecomp(s)
    return s if s < tmp else tmp

def gen(s):
    '''
    Randomly generate a sequence of ACTG of size s
    '''
    from random import randrange
    def aux():
        for i in range(s):
            rand = randrange(4)
            yield 'A' if rand==0 else 'C' if rand==1 else 'G' if rand==2 else 'T'
    return ''.join(list(aux()))
