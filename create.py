from utils import lexico

def create_dBG(In,k):
    '''
    return a python generator of kmer of size k from In.
    This version use by chuncks of letters.
    '''
    with open(In, 'r', 64000) as f:
        while True:
            c = f.read(1)
            if c == '':
                break
            if c == '>':
                while c!='\n':
                    c = f.read(1)
                    continue
            else:
                curr = c+f.read(k-1)
                while c!='\n':
                    yield lexico(curr)
                    curr = curr[1:]+c
                    c = f.read(1)


def create_dBG_chuncks(In,k):
    '''
    return a python generator of kmer of size k from In.
    This version read In line by line (can be slow on very long reads)
    '''
    with open(In) as f:
        for l in f:
            if l[0] == '>':
                continue
            for i in range(len(l)-k):
                curr = l[i:i+k]
                yield lexico(curr)
