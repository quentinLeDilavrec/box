from tempfile import TemporaryFile
import pickle
import os
from utils import lexico
from create import create_dBG_chuncks
from remover import remover
from bloom import Bloom

# '_' before a function is for private methods
# I want to acces it because it take most of the compressiong time, and it is the harder to optimize
def _make_comp(L,k,b,b_uniq_id):
    '''
    private,
    handle the building of the .comp file
    '''
    out=os.path.splitext(L)[0]+".comp"
    rsize = get_reads_size(L)
    with open(L) as f, open(out,"w+") as compressed_f:
        breakcount=0
        compressed_f.write(str(rsize)+" "+str(k)+" "+b_uniq_id+"\n") # save some info for decompression
        for read in f:
            if read[0]=='>':
                compressed_f.write(read)
                continue
            start, to_compress = read[:k], read[k:-1] if read[-1]=='\n' else read[k:]
            curr = start
            compressed=[]
            for nucleotide in to_compress:
                if not b[lexico(curr[1:]+nucleotide)]:
                    breakcount+=1
                    break
                branch_count = 0
                for e in ['A', 'C', 'T', 'G']:
                    if nucleotide!=e and b[lexico(curr[1:]+e)] :
                        branch_count+=1
                if branch_count>0:
                    compressed.append(nucleotide)
                curr = curr[1:]+nucleotide
            else: # no error (no break done in forloop)
                compressed_f.write(start+''.join(compressed)+'\n')
                continue
            compressed_f.write('!'+read)
    return out

import uuid
import time
def compress(L,s=10,k=31,T=2):
    '''
  compress:
      Un fichier .graph.pgz contenant une version compressée du graphe de de Bruijn (cf ensemble de kmer).
      Un fichier .comp étant une version compressée de L.
      The comment are kept but not compressed.
@param L: Le path vers un jeu de lectures au format fasta. Toutes les lectures étant de taille strictement identiques.
@param s: Multiplicateur de la taille du filtre de bloom en bits, de type int
@param k: La taille des k-mers, de type int
@param T: le seuil de solidité des kmers. Un k-mer dont le nombre d’occurrences est supérieur ou égal à cette valeur sont conservés dans le dBG. De type int
    '''
    assert(isinstance(L,str))
    assert(isinstance(s,int) or isinstance(s,float))
    assert(isinstance(k,int))
    assert(isinstance(T,int))

    # TODO count number of branches put in compressed file

    with open("temporary_kmers.out","w+") as temporary_kmers_f:
        print("kmerisation")
        temporary_kmers_f.writelines(( e+'\n' for e in create_dBG_chuncks(L,k)))

    print("filtering")
    size, filtered_kmer_f = remover("temporary_kmers.out",T) # get s, faillingkmer

    print("blooming")
    b_uniq_id = str(time.time()) + str(uuid.uuid4())
    b = Bloom(int(s * size))
    for e in filtered_kmer_f:
        b.set(e[:-1]) # remove \n before adding kmer to bloom filter
    filtered_kmer_f.close()
    with open(os.path.splitext(L)[0]+".graph.pgz","w+b") as f:
        pickle.dump((b_uniq_id,b),f,-1)

    print("compressing")
    out = _make_comp(L,k,b,b_uniq_id)
    print("finished")
    
    return out, os.path.splitext(L)[0]+".graph.pgz"

def get_reads_size(L):
    with open(L) as f:
        for l in f:
            if l[0]=='>':
                continue
            rsize = len(l[:-1] if l[-1]=='\n' else l)
            break
    return rsize

reads_file1="./tiny_ecoli_sample_perfect_20x_reads.fasta"
reads_file2="./ecoli_sample_perfect_reads_forward.fasta"
