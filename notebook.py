kmerfile=""
def nextkmer(kmer, k):
    r = []
    tmp = kmer[1:k]
    with open(kmerfile) as f:
        for l in f:
            if tmp == l[:k-1]:
                r.append(l)
    return r


def getchain(kmer, k):
    tmp = kmer
    while 1:
        yield tmp
        lst=nextkmer(tmp,k)
        if len(lst)==0: return
        tmp=max(set(lst), key=lst.count)

a = "TTCATGGCGCAATAACAACACGTTAATTGCTGAACGATTAATATGACCGAGCGTGGTCAGTATTTCGGCGAACAGGAACTGATGCGGCACATATTG"
lst = nextkmer(a,31)
print(max(set(lst), key=lst.count))
b = getchain(a,31)
try:
    for i in range(50):
       print(next(b))
except StopIteration as e:
    pass
