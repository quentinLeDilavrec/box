from tempfile import TemporaryFile
def remover(path,solidity=0):
    '''
    Return the numberr of kmer and the hook to a temporary file 
    containing a set of kmer occuring more than the solidity level
    and computed from the file at path. 
    '''
    pathnoext=path[:-3]
    import subprocess
    print("caution need to remove shell=true")
    subprocess.run("sort "+path+"| uniq -c > "+pathnoext+"cou", shell=True)
    kmer_counter=0
    dest = TemporaryFile("w+")
    with open(pathnoext+"cou") as src:
        for e in src:
            tmp = e.strip().split(" ",1)
            if int(tmp[0])>solidity:
                kmer_counter+=1
                dest.write(tmp[1]+"\n")
    dest.seek(0)
    return kmer_counter,dest
