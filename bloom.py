kmerfile=""
import numpy as np

class BitArray:
    '''An implementation of a bit array'''
    def __init__(self,size, cell_bits=3):
        self.cont = np.zeros(size,dtype="uint"+str(2**cell_bits))
        self.cell_bits = cell_bits

    def __getitem__(self, key):
        rem = key & (2**self.cell_bits-1)
        quo = key >> self.cell_bits
        return (((1 << rem) & self.cont[quo]) >> rem) & 0x1

    def set(self, key):
        rem = key & (2**self.cell_bits-1)
        quo = key >> self.cell_bits
        self.cont[quo] |= 1 << rem

    def __iter__(self):
        return (self[i] for i in range(self.cont.shape[0] * 8))

    def __len__(self):
        return self.cont.shape[0]*(2**self.cell_bits)

    def __str__(self):
        return ''.join((str(e) for e in self))

from Kmer2Hash import fast_kmer2hash as hashf


class Bloom:
    '''
    An implementation of a bloom filter
    ''' 
    def __init__(self,size, n_fcts=7):
        self.cont = BitArray(size)
        self.seeds = range(n_fcts)

    def __getitem__(self, key):
        for s in self.seeds:
            if not self.cont[hashf(key,s)%len(self)]:
                return False
        return True

    def set(self, key):
        for s in self.seeds:
            self.cont.set(hashf(key,s) % len(self))

    def __sizeof__(self):
        return len(self.cont)

    def __len__(self):
        return len(self.cont)

    def __str__(self):
        return self.cont.__str__()
