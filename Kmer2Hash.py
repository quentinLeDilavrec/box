
char2bitstr={"A":"00","C":"01","G":"10","T":"11"}

def kmer2hash(kmer,seed):
    return fast_kmer2hash(kmer,seed)
    # return kmer2hash_xorshift

def kmer2hash_xorshift (kmer,seed):
    key=int('0b' + ''.join([char2bitstr[c] for c in kmer]), 2)
    hash=seed
    hash ^= (hash <<  7) ^  key * (hash >> 3) ^ (~((hash << 11) + (key ^ (hash >> 5))));
    hash = (~hash) + (hash << 21);
    hash = hash ^ (hash >> 24);
    hash = (hash + (hash << 3)) + (hash << 8);
    hash = hash ^ (hash >> 14);
    hash = (hash + (hash << 2)) + (hash << 4);
    hash = hash ^ (hash >> 28);
    hash = hash + (hash << 31);
    return hash

############### Faster hash functions ###################@@
import hashlib
hashalgorithms=[]
for algo in hashlib.algorithms_guaranteed:
    hashalgorithms.append(algo)
hashalgorithms.sort()
max_seed=len(hashalgorithms)-1

def fast_kmer2hash(kmer,seed):
    ekmer = str.encode(kmer)
    if seed<=max_seed: 
        hash_object = eval("hashlib."+hashalgorithms[seed])(ekmer)
        # print(hash_object.hexdigest())
        return int(hash_object.hexdigest(),16)
    else: 
        hash_object1 = eval("hashlib."+hashalgorithms[seed%max_seed])(ekmer)
        hash_object2 = eval("hashlib."+hashalgorithms[(seed+1)%max_seed])(ekmer)
        return int(hash_object1.hexdigest(),16)+int(hash_object2.hexdigest(),16)
        
        