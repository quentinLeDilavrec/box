import os

def get_data(compressed,dBG):
    '''
    Get the usefull data froim the compressed file to later compare compression rates.
    '''
    with open(compressed) as compressed_f:
        rsize, k, saved_b_uniq_id = next(compressed_f).strip().split(" ")
        rsize = int(rsize)
        k= int(k)
        branches=[]
        n_reads_no_branches=0
        n_not_compressed=0
        n_reads=0
        for l in compressed_f:
            if l[0]=='>':
                continue
            n_reads+=1
            if l[0]=='!':
                n_not_compressed+=1
                continue
            lsize = len(l[:-1] if l[-1]=='\n' else l)
            tmp = lsize-k
            if tmp==0: n_reads_no_branches+=1
            else: branches.append(tmp)
    return rsize,k,branches,n_reads,n_reads_no_branches,n_not_compressed, os.path.getsize(compressed), os.path.getsize(dBG)

def compute_data(data,char_size,s):
    '''
an estimation of the size of the compressed file
if we compress the .comp in binary and
    '''
    rsize,k,branches,n_reads,n_reads_no_branches,n_not_compressed, comp_s, dBG_s = data
    tmp1 = char_size * (n_reads * k + sum(branches)) # estimate size of the compressed reads
    tmp2 = s * n_reads # estimate size of the bloom filter, don't take into account the overhead of the pickle format
    tmp3 = rsize * (char_size*n_not_compressed) # estimate size of the not compressed reads (starting with '!') 
    return (tmp1+tmp2+tmp3)
