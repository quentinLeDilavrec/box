import os
import pickle
from utils import lexico
from bloom import Bloom

def decompress(compressed,dBG):
    '''
  decompress:
      Get the original .fasta (reads with some infos) 
      by decompressing 'compressed' (1rst kmers and branches (and unchanged infos)) 
      using 'dBG' (a de Bruijn graph)
@param compressed: A path to a .comp being a compressed version of the corresponding .fasta.
@param dBG: A path to a .graph.pgz being a compressed version of the de Bruijn graph.
    '''
    assert(isinstance(compressed,str))
    assert(isinstance(compressed,str))

    with open(dBG,"rb") as f:
        b_uniq_id, b=pickle.load(f)
    out = os.path.splitext(compressed)[0]+".fasta.res"
    with open(compressed,"r") as compressed_f, open(out,"w+") as decompressed_f:
        rsize, k, saved_b_uniq_id = next(compressed_f).strip().split(" ")
        if not (b_uniq_id == saved_b_uniq_id):
            raise Exception('''
The bBG bloom filter '%s' is not compatible with the compressed file '%s'."
Respectivelly got '%s' and '%s' .
'''%(dBG,compressed,b_uniq_id,saved_b_uniq_id))
        rsize = int(rsize)  # the size of reads
        k = int(k)  # the size of kmers
        for read in compressed_f:
            if read[0]=='>':
                decompressed_f.write(read)
                continue
            if read[0]=='!':
                decompressed_f.write(read[1:])
                continue
            start, branches = read[:k], read[k:-1] if read[-1]=='\n' else read[k:]
            decompressed_f.write(start)
            i_branch=0
            curr=start
            for i in range(rsize-k):
                branch_count = 0
                choosed=''
                for e in ['A', 'C', 'T', 'G']:
                    if b[lexico(curr[1:]+e)] :
                        branch_count+=1
                        choosed=e
                if branch_count>1:
                    choosed=branches[i_branch]
                    i_branch+=1
                curr=curr[1:]+choosed
                decompressed_f.write(choosed)
            decompressed_f.write('\n')
    return out
